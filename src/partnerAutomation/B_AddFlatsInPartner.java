package partnerAutomation;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class B_AddFlatsInPartner {
	static WebDriver driver;
	static String foldername = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Partner";

	@BeforeMethod
	public void setUp() {
		driver = new FirefoxDriver();
	}

	@AfterMethod
	public void tearDown() {
		driver.close();
	}

	@Test
	public static void addingFlats() throws IOException, InterruptedException {
		AppiumServer.BaseTestCase.CreateFolder(foldername);
		driver.get("http://api.flatchat.com/partners/login/");
		WebDriverWait wait = new WebDriverWait(driver, 20);
		driver.manage().window().maximize();
		driver.findElement(By.id("id_username")).sendKeys(
				ResusableCodes.username);
		driver.findElement(By.id("id_password")).sendKeys(
				ResusableCodes.password);
		driver.findElement(By.xpath("//button[@type= 'submit']")).click();
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("admin-btn")));
			driver.findElement(By.id("admin-btn")).click();
			for (int i = 1; i <= 5; i++) {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By
						.id("add-new")));
				driver.findElement(By.id("add-new")).click();

				Select select = new Select(driver.findElement(By.id("type-")));
				select.selectByValue("4");
				driver.findElement(By.id("property-id-")).sendKeys("10" + i);
				Select select1 = new Select(driver.findElement(By
						.id("furnishing-")));
				select1.selectByValue("2");
				driver.findElement(By.id("available-from-")).sendKeys(
						"2015-11-30");
				driver.findElement(By.id("rent-")).sendKeys("1" + i + "000");
				driver.findElement(By.id("deposit-")).sendKeys("100000");
				driver.findElement(By.id("description_")).sendKeys(
						"Good Location, good society");
				driver.findElement(By.id("address")).clear();
				driver.findElement(By.id("address")).sendKeys("kanya");
				Thread.sleep(2000);
				driver.findElement(By.id("address")).sendKeys(Keys.ARROW_DOWN);
				driver.findElement(By.id("address")).sendKeys(Keys.TAB);

				driver.findElement(By.id("property-image-")).click();
				ResusableCodes.firstImage();
				Thread.sleep(2000);
				driver.findElement(By.id("property-image-")).click();
				ResusableCodes.secondImage();
				Thread.sleep(2000);
				driver.findElement(By.id("property-image-")).click();
				ResusableCodes.thirdImage();
				Thread.sleep(2000);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By
						.id("save-btn-modifier")));
				driver.findElement(By.id("save-btn-modifier")).click();

			}
			Thread.sleep(2000);
			File added = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(added,
					new File(foldername + "\\Flats_Added.png"));
			driver.findElement(By.id("property-id-1")).click();
			driver.findElement(By.id("flyer-btn")).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("download-flyer")));
			Thread.sleep(5000);
			File flyer = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(flyer, new File(foldername
					+ "\\Flyer_generated.png"));

		} catch (Exception e) {
			driver.close();
			System.out.println("Partner not added properly..!");

		}

		driver.close();

	}
}

package partnerAutomation;

import java.io.IOException;

import org.openqa.selenium.WebDriver;

public class ResusableCodes {

	static String username = "Happy";
	static String password = "12345";
	static String fName = "Happy";
	static String lName = "NewYear";
	static String logoName = "HappyLogo";
	static String email = "Partner@suppport.com";
	static WebDriver driver;

	public static void firstImage() throws InterruptedException, IOException {
		Runtime.getRuntime().exec(
				"E:/Automation_Setup/WorkSpace/FirstImage.exe");

	}

	public static void secondImage() throws InterruptedException, IOException {
		Runtime.getRuntime().exec(
				"E:/Automation_Setup/WorkSpace/SecondImage.exe");

	}

	public static void thirdImage() throws InterruptedException, IOException {
		Runtime.getRuntime().exec(
				"E:/Automation_Setup/WorkSpace/ThirdImage.exe");

	}

	public static void uploadNewImage() throws InterruptedException,
			IOException {
		Runtime.getRuntime().exec("E:/Automation_Setup/WorkSpace/NewImage.exe");

	}

	public static void uploadAppLogo() throws InterruptedException, IOException {
		Runtime.getRuntime().exec(
				"E:/Automation_Setup/WorkSpace/AppLogoUpload.exe");

	}

	public static void uploadFlyerLogo() throws InterruptedException,
			IOException {
		Runtime.getRuntime().exec(
				"E:/Automation_Setup/WorkSpace/FlyerLogoUpload.exe");

	}
}

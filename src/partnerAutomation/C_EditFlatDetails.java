package partnerAutomation;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class C_EditFlatDetails extends ResusableCodes {
	static WebDriver driver;
	static String foldername = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Partner";

	@BeforeMethod
	public void setUp() {
		driver = new FirefoxDriver();
	}

	@AfterMethod
	public void tearDown() {
		driver.close();
	}

	@Test
	public static void editFlats() throws IOException, InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		AppiumServer.BaseTestCase.CreateFolder(foldername);
		driver.get("http://dev.flat.to/partners/login/");
		driver.manage().window().maximize();
		driver.findElement(By.id("id_username")).sendKeys(
				ResusableCodes.username);
		driver.findElement(By.id("id_password")).sendKeys(
				ResusableCodes.password);
		driver.findElement(By.xpath("//button[@type= 'submit']")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("admin-btn")));
		driver.findElement(By.id("admin-btn")).click();
		driver.findElement(By.id("property-id-2")).click();
		driver.findElement(By.id("flyer-btn")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("download-flyer")));
		Thread.sleep(5000);
		File flyer_before = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(flyer_before, new File(foldername
				+ "\\Flyer_before_edit.png"));
		driver.findElement(By.className("close")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("edit-btn")).click();
		Thread.sleep(2000);
		driver.findElement(By.cssSelector("input[id*='available-from-']"))
				.clear();
		driver.findElement(By.cssSelector("input[id*='available-from-']"))
				.sendKeys("2015-12-30");
		driver.findElement(By.cssSelector("input[id*='rent-']")).clear();
		driver.findElement(By.cssSelector("input[id*='rent-']")).sendKeys(
				"12000");
		driver.findElement(By.cssSelector("input[id*='deposit-']")).clear();
		driver.findElement(By.cssSelector("input[id*='deposit-']")).sendKeys(
				"120000");
		driver.findElement(By.name("message")).clear();
		driver.findElement(By.name("message")).sendKeys(
				"Good Location, good society.. Well maintained Greenery");
		driver.findElement(By.className("add-btn-text")).click();
		ResusableCodes.uploadNewImage();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("save-btn-modifier")));
		driver.findElement(By.id("save-btn-modifier")).click();
		Thread.sleep(3000);
		File updated = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(updated, new File(foldername
				+ "\\Flyer_generated.png"));
		driver.findElement(By.id("property-id-2")).click();
		driver.findElement(By.id("flyer-btn")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("download-flyer")));
		Thread.sleep(5000);
		File flyer_after = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(flyer_after, new File(foldername
				+ "\\Flyer_after_edit.png"));

	}

}

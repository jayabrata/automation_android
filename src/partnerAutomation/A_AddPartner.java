package partnerAutomation;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class A_AddPartner {
	static WebDriver driver;
	static String foldername = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Partner";

	@BeforeMethod
	public void setUp() {
		driver = new FirefoxDriver();
	}

	@AfterMethod
	public void tearDown() {
		driver.close();
	}

	@Test
	public static void addPartner() throws InterruptedException, IOException {
		driver.get("http://dev.flat.to/backend/login/");
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("id_username")));
		driver.manage().window().maximize();
		driver.findElement(By.id("id_username")).sendKeys("admin");
		driver.findElement(By.id("id_password")).sendKeys("admin#123");
		driver.findElement(By.xpath("//button[@type= 'submit']")).click();
		driver.get("http://dev.flat.to/partners/add/");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("id_first_name")));

		driver.findElement(By.id("id_first_name")).sendKeys(
				ResusableCodes.fName);
		driver.findElement(By.id("id_last_name"))
				.sendKeys(ResusableCodes.lName);
		driver.findElement(By.id("id_password")).sendKeys(
				ResusableCodes.password);
		driver.findElement(By.id("id_repassword")).sendKeys(
				ResusableCodes.password);
		driver.findElement(By.id("id_email")).sendKeys(ResusableCodes.email);

		driver.findElement(By.id("id_applogoimage")).click();
		ResusableCodes.uploadAppLogo();
		Thread.sleep(2000);
		driver.findElement(By.id("id_logoname")).sendKeys(
				ResusableCodes.logoName);
		Thread.sleep(2000);
		driver.findElement(By.id("id_logoimage")).click();
		ResusableCodes.uploadFlyerLogo();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type= 'submit']")).click();
		Thread.sleep(3000);

	}
}

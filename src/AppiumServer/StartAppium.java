package AppiumServer;

import java.io.IOException;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;

public class StartAppium {

	public static void startServer() {
		CommandLine command = new CommandLine("cmd");
		System.out.println("-----Starting Appium Server-----");
		command.addArgument("/c");
		command.addArgument("E:/Automation_Setup/Appium/node.exe");
		command.addArgument("E:/Automation_Setup/Appium/node_modules/appium/bin/Appium.js");
		command.addArgument("--address");
		command.addArgument("127.0.0.1");
		command.addArgument("--port");
		command.addArgument("4723");
		command.addArgument("--no-reset");
		command.addArgument("--log");
		command.addArgument("E:/appiumLogs.txt");
		DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
		DefaultExecutor executor = new DefaultExecutor();
		executor.setExitValue(1);
		try {
			executor.execute(command, resultHandler);
			Thread.sleep(5000);
			System.out.println("-----Appium Server started Successfully-----");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/*
	 * public static void startServer2() { CommandLine command = new
	 * CommandLine("cmd");
	 * System.out.println("-----Starting Appium Server-----");
	 * command.addArgument("/c");
	 * command.addArgument("E:/Automation_Setup/Appium/node.exe");
	 * command.addArgument
	 * ("E:/Automation_Setup/Appium/node_modules/appium/bin/Appium.js");
	 * command.addArgument("--address"); command.addArgument("127.0.0.1");
	 * command.addArgument("--port"); command.addArgument("4723");
	 * command.addArgument("--no-reset"); command.addArgument("--log");
	 * command.addArgument("E:/appiumLogs.txt"); DefaultExecuteResultHandler
	 * resultHandler = new DefaultExecuteResultHandler(); DefaultExecutor
	 * executor = new DefaultExecutor(); executor.setExitValue(1); try {
	 * executor.execute(command, resultHandler); Thread.sleep(5000);
	 * System.out.println("-----Appium Server started Successfully-----"); }
	 * catch (IOException e) { e.printStackTrace(); } catch
	 * (InterruptedException e) { e.printStackTrace(); }
	 * 
	 * }
	 */

	public static void main(String[] args) {

		startServer();
		// startServer2();

	}
}
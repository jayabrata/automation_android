package AppiumServer;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeMethod;

public class BaseTestCase {

	protected AndroidDriver<AndroidElement> driver;
	public String Id_1 = "in.flatchat:id/budget_seekBar";
	public String Id_2 = "in.flatchat:id/duration_seekBar";

	@BeforeMethod
	public void setUp() throws MalformedURLException {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("device", "Android");
		capabilities.setCapability("VERSION", "5.1.1");
		capabilities.setCapability("deviceName", "D6502");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("appPackage", "in.flatchat");
		capabilities
				.setCapability("appActivity", ".ui.activity.SplashActivity");
		driver = new AndroidDriver<AndroidElement>(new URL(
				"http://127.0.0.1:4723/wd/hub"), capabilities);
	}

	public void DeleteTextFields(String Id) {
		driver.findElementById(Id).click();
		int rent = driver.findElementById(Id).getText().length();
		if (rent < 1) {
			System.out.println("Text Field Already cleared..!");
		} else {
			System.out.println(rent);
			for (int i = rent; i >= 0; i--) {
				driver.sendKeyEvent(67);
			}
		}
	}

	public int tapOnResult(String className) throws InterruptedException {
		WebElement element = driver.findElement(By
				.id("in.flatchat:id/autocomplete_location"));
		Point point = element.getLocation();
		int lenght = element.getSize().getWidth();
		// System.out.println(lenght);
		int height = element.getSize().getHeight();
		// System.out.println(height);
		int getY = point.getY();
		// System.out.println(getY);
		int middleY = (int) (getY + height * 1.5);
		// System.out.println(middleY);
		TouchAction ta = new TouchAction(driver);
		Thread.sleep(1000);
		ta.tap(lenght / 2, middleY).perform();
		return middleY;

	}

	public static void CreateFolder(String folderName) {
		File file = new File(folderName);
		if (!file.exists()) {
			if (file.mkdir()) {
				System.out.println("Directory is created!");
			} else {
				System.out.println("Failed to create directory!");
			}
		}

	}

	public int getX(String idx) {
		WebElement element = driver.findElement(By.id(idx));
		Point point = element.getLocation();
		org.openqa.selenium.Dimension size = element.getSize();
		int elementCenterX = point.getX() + Math.round(size.getWidth() / 2);
		System.out.println(point.getX());
		return elementCenterX;

	}

	public int getY(String idy) {
		WebElement element = driver.findElement(By.id(idy));
		Point point = element.getLocation();
		org.openqa.selenium.Dimension size = element.getSize();
		int elementCenterY = point.getY() + Math.round(size.getHeight() / 2);
		System.out.println(elementCenterY);
		return elementCenterY;

	}

	public int getStartX1() {
		WebElement element = driver.findElement(By.id(Id_1));
		Point point = element.getLocation();
		int startX = point.getX();
		return startX;
	}

	public int getStartX2() {
		WebElement element = driver.findElement(By.id(Id_2));
		Point point = element.getLocation();
		int startX = point.getX();
		return startX;
	}

	public void submitRating_negative(String className)
			throws InterruptedException {
		WebElement element = driver.findElement(By
				.id("in.flatchat:id/ratingBar"));
		Point point = element.getLocation();
		int length = element.getSize().getWidth();
		int height = element.getSize().getHeight();
		// int getX = point.getX();
		int getY = point.getY();
		System.out.println(length);
		System.out.println(height);
		System.out.println(length - 100);
		System.out.println(getY + height / 2);
		TouchAction ta = new TouchAction(driver);
		Thread.sleep(1000);
		ta.tap(length, getY + height / 2).perform();

	}

	public void submitRating_feedback(String className)
			throws InterruptedException {
		WebElement element = driver.findElement(By.id("in.flatchat:id/rating"));
		Point point = element.getLocation();
		int length = element.getSize().getWidth();
		int height = element.getSize().getHeight();
		// int getX = point.getX();
		int getY = point.getY();
		// System.out.println(length);
		// System.out.println(height);
		// System.out.println(length - 100);
		// System.out.println(getY + height / 2);
		TouchAction ta = new TouchAction(driver);
		Thread.sleep(1000);
		ta.tap(length + 100, getY + height / 2).perform();

	}

	public void swipeUsingCoOrdinate(String ID) {
		WebElement element = driver.findElement(By.id(ID));
		Point point = element.getLocation();
		int length = element.getSize().getWidth();
		int height = element.getSize().getHeight();
		// System.out.println("" + length + "" + height);
		int Y = point.getY();
		// System.out.println(Y);
		driver.swipe(length, height / 2, length - 100, height, 2);
	}

	public void deleteProfileVideo(String ID) {
		WebElement element = driver.findElement(By.id(ID));
		// Point point = element.getLocation();
		int length = element.getSize().getWidth();
		// System.out.println(length);
		int height = element.getSize().getHeight();
		// System.out.println(height);
		Point point = element.getLocation();
		int startX = point.getX();
		// System.out.println(startX);
		int endX = startX + length;
		// System.out.println(endX);
		int elementCenterY = point.getY() + height / 2;
		// System.out.println(point.getY());
		// System.out.println(elementCenterY);
		TouchAction ta = new TouchAction(driver);
		ta.tap(endX - 10, elementCenterY).perform();
	}
}

package seekerProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import seleniumPart.SendingSevenDayNotofication;
import AppiumServer.BaseTestCase;

public class R_SevenDay_Notification extends BaseTestCase {
	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Seeker\\R_SevenDay_Notification";

	public R_SevenDay_Notification() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		super.setUp();

	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Test
	public void openUsingYesOption() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("CHATS")));
		driver.sendKeyEvent(3);
		Thread.sleep(2000);
		driver.openNotifications();
		try {
			driver.findElementById("com.android.systemui:id/dismiss_text")
					.click();
			Thread.sleep(2000);
			driver.sendKeyEvent(3);
		} catch (Exception e) {
			driver.sendKeyEvent(3);
		}
		SendingSevenDayNotofication.sedingNotificationToDevice("Jayabrata");
		driver.openNotifications();
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.name("We have missed you!")));
			File image1 = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(image1, new File(folderPath
					+ "\\Notification_received.png"));
			driver.findElementByName("YES").click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.name("CHATS")));
			System.out.println("Notification received.. App Opened..!");
		} catch (Exception e) {
			System.out.println("Notification not received..!");
		}

	}

}

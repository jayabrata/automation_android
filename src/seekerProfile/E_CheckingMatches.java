package seekerProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class E_CheckingMatches extends BaseTestCase {
	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Seeker\\E_CheckingMatches";
	public String tab_1 = "TENANTS";
	public String tab_2 = "OWNERS";
	public String tab_3 = "SEEKERS";
	public String tab_4 = "CHATS";

	public E_CheckingMatches() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Test
	public void matchesView() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("CHATS")));
		for (int i = 1; i <= 3; i++) {

			if (i == 1) {
				driver.findElementByName("TENANTS").click();
				File image1 = ((TakesScreenshot) driver)
						.getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(image1, new File(folderPath
						+ "\\Seeker_Matches_" + i + ".png"));
				try {
					driver.findElementById("in.flatchat:id/btn_got_it").click();
				} catch (Exception e) {
				}
			}
			if (i == 2) {
				driver.findElementByName("OWNERS").click();
				File image2 = ((TakesScreenshot) driver)
						.getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(image2, new File(folderPath
						+ "\\Seeker_Matches_" + i + ".png"));
				try {
					driver.findElementById("in.flatchat:id/btn_got_it").click();
				} catch (Exception e) {

				}
			}

			if (i == 3) {
				driver.findElementByName("SEEKERS").click();
				File image3 = ((TakesScreenshot) driver)
						.getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(image3, new File(folderPath
						+ "\\Seeker_Matches_" + i + ".png"));
				try {
					driver.findElementById("in.flatchat:id/btn_got_it").click();
				} catch (Exception e) {

				}
			}
		}
	}
}

package seekerProfile;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidElement;

import java.net.MalformedURLException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class M_DeleteChat extends BaseTestCase {
	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Seeker\\M_DeleteChat";

	public M_DeleteChat() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Test
	public void deleteChat() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("CHATS")));
		driver.findElementByName("CHATS").click();

		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("in.flatchat:id/introduce_text")));
			driver.findElementById("in.flatchat:id/btn_got_it").click();
		} catch (Exception e) {
		}

		List<AndroidElement> names = driver.findElements(By
				.xpath("//android.widget.ImageView"));
		try {
			((RemoteWebElement) names.get(2)).click();
		} catch (Exception e) {
			((RemoteWebElement) names.get(1)).click();
		}
		String nameInToolbar = driver.findElementById(
				"in.flatchat:id/toolbar_title").getText();
		driver.findElementByAccessibilityId("Navigate up").click();
		TouchAction ta = new TouchAction(driver);
		System.out.println(nameInToolbar);
		Thread.sleep(2000);
		ta.longPress(driver.findElementByName(nameInToolbar)).perform();
		try {
			driver.findElementByName("Delete Chat").click();
		} catch (Exception e) {
			driver.findElementByName("Leave Group").click();
		}
		Thread.sleep(2000);

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By
				.name(nameInToolbar)));
		ta.longPress(driver.findElementByName(nameInToolbar)).perform();
		driver.findElementByName("Delete Chat").click();

	}
}
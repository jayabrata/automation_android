package seekerProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class C_CreateProfile extends BaseTestCase {
	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Seeker\\C_CreateProfile";

	public C_CreateProfile() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	public void DeleteTextFields(String Id) {
		super.DeleteTextFields(Id);
	}

	@Override
	public int tapOnResult(String className) throws InterruptedException {
		return super.tapOnResult(className);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Parameters({ "Country" })
	@Test
	public void runCreateProifle(String c) throws Exception {

		if (c.equals("India")) {
			CreateProfile_IN();
		} else {
			CreateProfile_SG();
		}
	}

	public void CreateProfile_IN() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageButton[@index='0']")));
		driver.findElementByAccessibilityId("Flatchat").click();

		try {
			driver.findElementByName("Edit Profile").click();
		} catch (Exception e) {
			driver.findElementByName("Create Profile").click();
		}
		driver.findElementById("in.flatchat:id/btn_2").click();
		int startX1 = getStartX1();
		int X1 = getX(Id_1);
		int Y1 = getY(Id_1);
		driver.swipe(startX1, Y1, X1 / 2, Y1, 1000);

		driver.findElement(By.id("in.flatchat:id/location_boxA")).click();
		Thread.sleep(3000);
		driver.findElementById("in.flatchat:id/right_cross").click();
		driver.findElementById("in.flatchat:id/autocomplete_location")
				.sendKeys("Kanyakumari");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.className("android.view.View")));
		tapOnResult("android.view.View");
		Thread.sleep(3000);
		driver.findElementById("in.flatchat:id/loc_done").click();
		driver.scrollTo("INTERESTS");
		/*
		 * int startX2 = getStartX2(); int X2 = getX(Id_2);
		 * System.out.println(X2 + X2); int Y2 = getY(Id_2);
		 * driver.swipe(startX2, Y2, X2 + X2 / 2, Y2, 2000);
		 */
		driver.findElementByName("Working Professional").click();
		driver.findElementByName("Techie").click();
		driver.findElementByName("Party-Lover").click();
		driver.scrollTo("Are you fine with PG accommodation?");
		driver.findElementById("in.flatchat:id/pg_switch").click();
		driver.findElementById("in.flatchat:id/save_menu").click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/share_flyer")));
		File image2 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image2, new File(folderPath
				+ "\\Seeker_profileCreated.png"));

	}

	public void CreateProfile_SG() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageButton[@index='0']")));
		driver.findElementByAccessibilityId("Flatchat").click();

		try {
			driver.findElementByName("Edit Profile").click();
		} catch (Exception e) {
			driver.findElementByName("Create Profile").click();
		}
		driver.findElementById("in.flatchat:id/btn_2").click();
		int startX1 = getStartX1();
		int X1 = getX(Id_1);
		int Y1 = getY(Id_1);
		driver.swipe(startX1, Y1, X1 / 4, Y1, 1000);

		driver.findElement(By.id("in.flatchat:id/location_boxA")).click();
		Thread.sleep(3000);
		driver.findElementById("in.flatchat:id/right_cross").click();
		driver.findElementById("in.flatchat:id/autocomplete_location")
				.sendKeys("Victoria");
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.className("android.view.View")));
		tapOnResult("android.view.View");
		Thread.sleep(3000);
		driver.findElementById("in.flatchat:id/loc_done").click();
		driver.scrollTo("INTERESTS");
		/*
		 * int startX2 = getStartX2(); int X2 = getX(Id_2);
		 * System.out.println(X2 + X2); int Y2 = getY(Id_2);
		 * driver.swipe(startX2, Y2, X2 + X2 / 2, Y2, 2000);
		 */
		driver.findElementByName("Working Professional").click();
		driver.findElementByName("Techie").click();
		driver.findElementByName("Party-Lover").click();
		driver.findElementById("in.flatchat:id/save_menu").click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/share_flyer")));
		File image = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image, new File(folderPath
				+ "\\Seeker_profileCreatedSG.jpg"));

	}
}
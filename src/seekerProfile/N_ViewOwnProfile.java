package seekerProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class N_ViewOwnProfile extends BaseTestCase {
	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Seeker\\N_ViewOwnProfile";
	public String Id = "in.flatchat:id/budget_seekBar";

	public N_ViewOwnProfile() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Test
	public void blockUnblockUser() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageButton[@index='0']")));
		driver.findElementByAccessibilityId("Flatchat").click();
		driver.findElementById("in.flatchat:id/ppic").click();
		driver.scrollTo("INTERESTS");
		File image = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image, new File(folderPath + "\\OwnProfile.jpg"));
		String beforeEdit = driver.findElementById("in.flatchat:id/budget_val")
				.getText();
		driver.findElementByAccessibilityId("More options").click();
		driver.findElementByName("Edit Profile").click();
		int startX = getStartX1();
		int X = getX(Id);
		int Y = getY(Id);
		driver.swipe(startX, Y, X / 2, Y, 1000);
		driver.findElementById("in.flatchat:id/save_menu").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/action_close")));

		driver.findElementById("in.flatchat:id/action_close").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageButton[@index='0']")));
		driver.findElementByAccessibilityId("Flatchat").click();
		driver.findElementById("in.flatchat:id/ppic").click();
		String afterEdit = driver.findElementById("in.flatchat:id/budget_val")
				.getText();
		if (afterEdit != beforeEdit) {
			File image1 = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(image1, new File(folderPath
					+ "\\Edited_profile.jpg"));
			System.out.println("Profile edited successfully");
		} else {
			System.out.println("Profile not edited properly");
		}

	}
}
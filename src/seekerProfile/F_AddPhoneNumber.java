package seekerProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import seleniumPart.VerificationCodeRetrieve;
import AppiumServer.BaseTestCase;

public class F_AddPhoneNumber extends BaseTestCase {

	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Seeker\\F_AddPhoneNumber";

	public F_AddPhoneNumber() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Parameters({ "Country" })
	@Test
	public void runAddPhoneNumber(String c) throws Exception {

		if (c.equals("India")) {
			addphoneNumber();
		} else {
			PhoneVerification();
		}
	}

	void addphoneNumber() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("CHATS")));
		driver.findElementByAccessibilityId("Flatchat").click();
		try {
			driver.findElementByName("Add Phone Number").click();
			driver.findElementById("in.flatchat:id/phone").sendKeys(
					"7483121154");
			driver.findElementById("in.flatchat:id/add_phone_submit_btn")
					.click();
			String code = VerificationCodeRetrieve.retrieveCode("7483121154");
			driver.findElementByName("Enter Verification Code").sendKeys(code);
			driver.findElementByName("Verify").click();
		} catch (Exception e) {
			try {
				driver.findElementByName("Verify Phone Number").click();
				String code = VerificationCodeRetrieve
						.retrieveCode("7483121154");
				driver.findElementByName("Enter Verification Code").sendKeys(
						code);
				driver.findElementByName("Verify").click();
			} catch (Exception e1) {
				System.out
						.println("Phone number already added. Option not present!!");
			}

		}
		Thread.sleep(2000);

		File image = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image, new File(folderPath
				+ "\\Seeker_PhoneNumber.png"));
		driver.sendKeyEvent(82);

	}

	public void PhoneVerification() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.elementToBeClickable(By.name("CHATS")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageButton[@index='0']")));
		driver.findElementByAccessibilityId("Flatchat").click();
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.name("Verify Phone Number")));
			driver.findElementByName("Verify Phone Number").click();
			String code = VerificationCodeRetrieve.retrieveCode("81234567");
			driver.findElementByName("Enter Verification Code").sendKeys(code);
			driver.findElementByName("Verify").click();
		} catch (Exception e) {
			System.out.println("Phone number already verified..!");
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageButton[@index='0']")));
		driver.findElementByAccessibilityId("Flatchat").click();
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.name("Verify Phone Number")));
		} catch (Exception e1) {
			System.out.println("Phone number veerification successfull...!");
		}

	}
}
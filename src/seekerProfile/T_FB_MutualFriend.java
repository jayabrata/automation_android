package seekerProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class T_FB_MutualFriend extends BaseTestCase {
	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Seeker\\T_FB_MutualFriend";
	public String tab_1 = "TENANTS";
	public String tab_2 = "OWNERS";
	public String tab_3 = "SEEKERS";
	public String tab_4 = "CHATS";

	public T_FB_MutualFriend() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Test
	public void MutualFriends() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("CHATS")));

		try {
			File image1 = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			try {
				driver.findElementById("in.flatchat:id/btn_got_it").click();
				driver.findElementById("in.flatchat:id/friend_logo").click();
				driver.scrollToExact("MUTUAL FRIENDS");
				FileUtils.copyFile(image1, new File(folderPath
						+ "\\Seeker_MutualFriends.png"));
				driver.findElementByAccessibilityId("Navigate up").click();
			} catch (Exception e) {
				driver.findElementById("in.flatchat:id/friend_logo").click();
				driver.scrollToExact("MUTUAL FRIENDS");
				FileUtils.copyFile(image1, new File(folderPath
						+ "\\Seeker_MutualFriends.png"));
				driver.findElementByAccessibilityId("Navigate up").click();
			}
		} catch (Exception e) {
			System.out.println(e);
		}

		driver.findElementByName("OWNERS").click();
		try {
			File image2 = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			try {
				driver.findElementById("in.flatchat:id/btn_got_it").click();
				driver.findElementById("in.flatchat:id/friend_logo").click();
				driver.scrollToExact("MUTUAL FRIENDS");
				FileUtils.copyFile(image2, new File(folderPath
						+ "\\Owner_MutualFriends.png"));
				driver.findElementByAccessibilityId("Navigate up").click();
			} catch (Exception e) {
				driver.findElementById("in.flatchat:id/friend_logo").click();
				driver.scrollToExact("MUTUAL FRIENDS");
				FileUtils.copyFile(image2, new File(folderPath
						+ "\\Owner_MutualFriends.png"));
				driver.findElementByAccessibilityId("Navigate up").click();
			}
		} catch (Exception e) {
		}
		driver.findElementByName("SEEKERS").click();
		try {
			File image3 = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);

			try {
				driver.findElementById("in.flatchat:id/btn_got_it").click();
				driver.findElementById("in.flatchat:id/friend_logo").click();
				driver.scrollToExact("MUTUAL FRIENDS");
				FileUtils.copyFile(image3, new File(folderPath
						+ "\\Seeker_MutualFriends.png"));
			} catch (Exception e) {
				driver.findElementById("in.flatchat:id/friend_logo").click();
				driver.scrollToExact("MUTUAL FRIENDS");
				FileUtils.copyFile(image3, new File(folderPath
						+ "\\Seeker_MutualFriends.png"));
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}

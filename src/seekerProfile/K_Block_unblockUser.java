package seekerProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class K_Block_unblockUser extends BaseTestCase {
	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Seeker\\K_Block_unblockUser";

	public K_Block_unblockUser() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Test
	public void blockUnblockUser() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("CHATS")));
		driver.findElementByName("CHATS").click();
		driver.scrollToExact("Hello there, can we chat?");
		driver.findElementByName("Hello there, can we chat?").click();
		Thread.sleep(2000);
		File image1 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils
				.copyFile(image1, new File(folderPath + "\\User_to_block.png"));
		String nameInToolbar = driver.findElementById(
				"in.flatchat:id/toolbar_title").getText();
		driver.findElementByAccessibilityId("More options").click();
		Thread.sleep(2000);
		driver.findElementByName("Block user").click();
		driver.findElementByAccessibilityId("Navigate up").click();
		try {
			driver.findElementByAccessibilityId("Flatchat").click();
		} catch (Exception e) {
			driver.findElementByName("SEEKERS").click();
			driver.findElementByName("OWNERS").click();
			driver.findElementByAccessibilityId("Flatchat").click();
		}
		driver.findElementByName("Blocked Users").click();
		File image = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image, new File(folderPath + "\\Blocked_user.png"));
		driver.findElementByName(nameInToolbar).click();
		driver.findElementByAccessibilityId("Navigate up").click();
		driver.findElementByName("CHATS").click();
		driver.scrollToExact(nameInToolbar);

		File image2 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image2,
				new File(folderPath + "\\Unblocked_user.png"));

	}
}
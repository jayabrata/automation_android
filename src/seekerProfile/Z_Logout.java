package seekerProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class Z_Logout extends BaseTestCase {

	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Seeker\\Z_Logout";

	public Z_Logout() {
		BaseTestCase.CreateFolder(folderPath);

	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		// TODO Auto-generated method stub
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
		AppiumServer.StopAppium.stopServer();
		AppiumServer.StartAppium.startServer();
	}

	@Test
	public void LogoutSeeker() throws Exception {
		Thread.sleep(5000);
		driver.findElementByAccessibilityId("Flatchat").click();
		driver.findElementByName("Settings").click();
		driver.findElementByName("Logout").click();
		File image = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image, new File(folderPath + "\\LogoutPopUp.png"));
		driver.findElementById("android:id/button2").click();
		Thread.sleep(10000);
		File image1 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image1, new File(folderPath + "\\Logged_out.png"));
		seleniumPart.PatronDelete.deleteOfPatron();
	}
}
package seekerProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import seleniumPart.SendingSevenDayNotofication;
import AppiumServer.BaseTestCase;

public class S_SevenDayNotification_NegativeAction extends BaseTestCase {
	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Seeker\\S_SevenDayNotification_NegativeAction";

	public S_SevenDayNotification_NegativeAction() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		super.setUp();

	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Test
	public void closeUsingNoOption() throws Exception {
		found();
		foundElsewhere();
		badExp();

	}

	public void found() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("CHATS")));
		driver.sendKeyEvent(3);
		Thread.sleep(2000);
		driver.openNotifications();
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("com.android.systemui:id/dismiss_text")));
			driver.findElementById("com.android.systemui:id/dismiss_text")
					.click();
			Thread.sleep(2000);
			driver.sendKeyEvent(3);
		} catch (Exception e) {
			driver.sendKeyEvent(3);
		}
		SendingSevenDayNotofication.sedingNotificationToDevice("Jayabrata");
		driver.openNotifications();
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.name("We have missed you!")));
			File image1 = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(image1, new File(folderPath
					+ "\\Notification_received.png"));
			driver.findElementByName("NO").click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.name("Submit")));
			driver.findElementByName(
					"I found what i was looking for on Flatchat").click();
			driver.findElementByName("Submit").click();
			submitRating_negative("android.widget.RatingBar");
			driver.findElementByName("Done").click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.name("Your discovery has been turned off. Other users won't be able to view your profile.")));
			File image2 = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(image2, new File(folderPath
					+ "\\Feedback_Submitted.png"));
			driver.sendKeyEvent(4);
			try {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By
						.name("Your Profile is hidden!")));
				driver.sendKeyEvent(3);
			} catch (Exception e1) {
				System.out
						.println("Profile is not hidden after feedback submission");
			}

		} catch (Exception e) {
			System.out.println("Notification not received..!");
		}

	}

	public void foundElsewhere() throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 10);
		driver.openNotifications();
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("com.android.systemui:id/dismiss_text")));
			driver.findElementById("com.android.systemui:id/dismiss_text")
					.click();
			Thread.sleep(2000);

		} catch (Exception e) {
			driver.sendKeyEvent(3);
		}
		SendingSevenDayNotofication.sedingNotificationToDevice("Jayabrata");
		driver.openNotifications();
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.name("We have missed you!")));
			driver.findElementByName("NO").click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.name("Submit")));
			driver.findElementByName("I found my requirement elsewhere")
					.click();
			driver.findElementByName("Submit").click();
			driver.findElementById("in.flatchat:id/feedback_text").sendKeys(
					"Feedback Submitted");
			driver.hideKeyboard();
			File image3 = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(image3, new File(folderPath
					+ "\\Feedback_Submitting.png"));
			driver.findElementById("in.flatchat:id/submit_btn").click();
			try {
				driver.startActivity("in.flatchat",
						".ui.activity.SplashActivity");
				wait.until(ExpectedConditions.visibilityOfElementLocated(By
						.name("Your Profile is hidden!")));
				driver.sendKeyEvent(4);
			} catch (Exception e1) {
				driver.startActivity("in.flatchat",
						".ui.activity.SplashActivity");
				wait.until(ExpectedConditions.visibilityOfElementLocated(By
						.name("Your Profile is hidden!")));
			}

		} catch (Exception e) {
			System.out
					.println("Profile is not hidden after feedback Submission..!");
		}

	}

	public void badExp() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		driver.openNotifications();
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("com.android.systemui:id/dismiss_text")));
			driver.findElementById("com.android.systemui:id/dismiss_text")
					.click();
			Thread.sleep(2000);

		} catch (Exception e) {
			driver.sendKeyEvent(3);
		}
		SendingSevenDayNotofication.sedingNotificationToDevice("Jayabrata");
		driver.openNotifications();
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.name("We have missed you!")));
			driver.findElementByName("NO").click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.name("Submit")));
			driver.findElementByName("I didn't like the App experience")
					.click();
			driver.findElementByName("Submit").click();
			driver.findElementById("in.flatchat:id/feedback_text").sendKeys(
					"Need improvement on App");
			driver.hideKeyboard();
			File image4 = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(image4, new File(folderPath
					+ "\\Feedback_Submitting_1.png"));
			driver.findElementById("in.flatchat:id/submit_btn").click();
			try {
				driver.startActivity("in.flatchat",
						".ui.activity.SplashActivity");
				wait.until(ExpectedConditions.visibilityOfElementLocated(By
						.name("Your Profile is hidden!")));
				driver.sendKeyEvent(4);
			} catch (Exception e1) {
				driver.startActivity("in.flatchat",
						".ui.activity.SplashActivity");
				wait.until(ExpectedConditions.visibilityOfElementLocated(By
						.name("Your Profile is hidden!")));
			}

		} catch (Exception e) {
			System.out
					.println("Profile is not hidden after feedback Submission..!");
		}

	}
}

package seekerProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class G_Discovery_Off_On extends BaseTestCase {

	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Seeker\\G_Discovery_Off_On";

	public G_Discovery_Off_On() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Test
	public void discoveryStateChange() throws Exception {
		Thread.sleep(5000);
		driver.findElementByAccessibilityId("Flatchat").click();
		driver.findElementById("in.flatchat:id/discovery_switch").click();
		driver.findElementByName("Ok").click();
		Thread.sleep(2000);
		File image = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image, new File(folderPath
				+ "\\Discovery_Changed_1.png"));

		driver.findElementByName("CHATS").click();
		Thread.sleep(2000);
		File image1 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils
				.copyFile(image1, new File(folderPath + "\\ProfileHidden.png"));

		driver.findElementByAccessibilityId("Flatchat").click();
		driver.findElementById("in.flatchat:id/discovery_switch").click();
		driver.findElementByName("Ok").click();
		Thread.sleep(2000);
		File image2 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image2, new File(folderPath
				+ "\\Discovery_Changed_1.png"));

		driver.findElementByName("CHATS").click();
		Thread.sleep(2000);
		File image4 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils
				.copyFile(image4, new File(folderPath + "\\ProfileActive.png"));

		try {
			driver.findElementByName("Your Profile is hidden!").click();
			driver.findElementByAccessibilityId("Flatchat").click();
			driver.findElementById("in.flatchat:id/discovery_switch").click();
			driver.findElementByName("Ok").click();
			Thread.sleep(2000);
			driver.sendKeyEvent(4);

		} catch (Exception e) {
			driver.sendKeyEvent(4);
		}
	}
}

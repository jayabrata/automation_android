package seekerProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class D_EditProfileSeeker extends BaseTestCase {
	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Seeker\\D_EditProfileSeeker";
	public String Id = "in.flatchat:id/budget_seekBar";

	public D_EditProfileSeeker() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Parameters({ "Country" })
	@Test
	public void runCreateProifle(String c) throws Exception {

		if (c.equals("India")) {
			editProfile_IN();
		} else {
			editProfile_SG();
		}
	}

	public void editProfile_IN() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageButton[@index='0']")));
		driver.findElementByXPath("//android.widget.ImageButton[@index='0']")
				.click();
		driver.findElementByName("Flyer").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/cancel")));
		File image1 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image1, new File(folderPath
				+ "\\Seeker_FlyerBeforeEditProfile.png"));

		driver.findElementById("in.flatchat:id/cancel").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageButton[@index='0']")));
		driver.findElementByXPath("//android.widget.ImageButton[@index='0']")
				.click();
		driver.findElementByName("Edit Profile").click();
		int startX = getStartX1();
		int X = getX(Id);
		int Y = getY(Id);
		driver.swipe(startX, Y, X / 3, Y, 1000);
		driver.findElementById("in.flatchat:id/btn_3").click();
		driver.scrollTo("INTERESTS");
		driver.findElementByName("Working Professional").click();
		driver.findElementByName("Techie").click();
		// driver.findElementByName("Party-Lover").click();
		driver.scrollTo("Are you fine with PG accommodation?");
		driver.findElementById("in.flatchat:id/pg_switch").click();
		driver.findElementById("in.flatchat:id/save_menu").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/cancel")));

		File image2 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image2, new File(folderPath
				+ "\\Seeker_FlyerAfterEditProfile.png"));

		driver.findElementById("in.flatchat:id/cancel").click();
	}

	public void editProfile_SG() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageButton[@index='0']")));
		driver.findElementByAccessibilityId("Flatchat").click();

		try {
			driver.findElementByName("Edit Profile").click();
		} catch (Exception e) {
			driver.findElementByName("Create Profile").click();
		}
		driver.findElementById("in.flatchat:id/btn_1").click();
		driver.findElementById("in.flatchat:id/radioBoth").click();
		int startX1 = getStartX1();
		int X1 = getX(Id_1);
		int Y1 = getY(Id_1);
		driver.swipe(startX1, Y1, X1 / 4, Y1, 1000);
		Thread.sleep(3000);
		driver.scrollTo("INTERESTS");
		driver.findElementByName("Working Professional").click();
		driver.findElementByName("Techie").click();
		// driver.findElementByName("Party-Lover").click();
		driver.findElementById("in.flatchat:id/save_menu").click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/share_flyer")));
		File image2 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image2, new File(folderPath
				+ "\\Seeker_profileEditedSG.jpg"));

	}
}
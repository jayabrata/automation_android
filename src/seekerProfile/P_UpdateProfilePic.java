package seekerProfile;

import io.appium.java_client.android.AndroidElement;

import java.io.File;
import java.net.MalformedURLException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class P_UpdateProfilePic extends BaseTestCase {
	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Seeker\\P_UpdateProfilePic";
	public String Id = "com.sonyericsson.album:id/picker";

	public P_UpdateProfilePic() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Test
	public void updateProfilePic() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageButton[@index='0']")));
		driver.findElementByAccessibilityId("Flatchat").click();
		driver.findElementById("in.flatchat:id/ppic").click();
		File image = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image,
				new File(folderPath + "\\Pic_before_edit.jpg"));
		driver.findElementByAccessibilityId("More options").click();
		driver.findElementByName("Change Profile Picture").click();
		driver.findElementByName("Select Photo").click();
		driver.findElementByName("AllPhotos").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageView")));
		List<AndroidElement> images = driver.findElements(By
				.xpath("//android.widget.ImageView"));
		((RemoteWebElement) images.get(3)).click();

		driver.findElementById("in.flatchat:id/crp_done").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/upload")));
		driver.findElementById("in.flatchat:id/upload").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageButton[@index='0']")));
		driver.findElementByAccessibilityId("Flatchat").click();
		Thread.sleep(2000);
		File image1 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image1, new File(folderPath
				+ "\\Edited_profilePic.jpg"));

	}
}
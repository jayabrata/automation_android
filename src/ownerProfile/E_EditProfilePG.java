package ownerProfile;

import io.appium.java_client.android.AndroidElement;

import java.net.MalformedURLException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class E_EditProfilePG extends BaseTestCase {

	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Owner\\E_EditProfilePG";
	String Gender = "Male";

	public E_EditProfilePG() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	public void DeleteTextFields(String Id) {
		super.DeleteTextFields(Id);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Parameters({ "Country" })
	@Test
	public void runCreateProifle(String c) throws Exception {
		if (c.equals("India")) {
			editProfile_PG();
		} else {
			EditProfile_Rooms();
		}
	}

	public void editProfile_PG() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageButton[@index='0']")));

		driver.findElementByAccessibilityId("Flatchat").click();
		try {
			driver.findElementByName("Edit Profile").click();
		} catch (Exception e) {
			driver.findElementByName("Create Profile").click();

		}
		driver.findElementById("in.flatchat:id/type_pg").click();
		driver.findElementById("in.flatchat:id/radioBoth").click();
		driver.findElementById("in.flatchat:id/upload_image_btn").click();
		driver.findElementByName("Choose from Gallery").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("AllPhotos")));
		driver.findElementByName("AllPhotos").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageView")));

		List<AndroidElement> images = driver.findElements(By
				.xpath("//android.widget.ImageView"));
		((RemoteWebElement) images.get(2)).click();
		((RemoteWebElement) images.get(4)).click();
		((RemoteWebElement) images.get(5)).click();
		System.out.println(images.size());
		driver.findElementById("in.flatchat:id/btnGalleryOk").click();

		driver.findElementById("in.flatchat:id/loc_done").click();
		try {
			driver.scrollToExact("Monthly Rent");
		} catch (Exception e) {
			System.out.println("Rent alrady added..!");
		}
		driver.findElementById("in.flatchat:id/ss_rent_val").sendKeys("6000");
		try {
			driver.hideKeyboard();
		} catch (Exception e) {
			System.out.println("Keyboard is already hidden..!");
		}
		driver.findElementById("in.flatchat:id/ss_deposit_val").sendKeys(
				"18000");
		driver.findElementById("in.flatchat:id/ds_rent_switch").click();
		driver.findElementById("in.flatchat:id/ds_rent_val").sendKeys("5000");
		driver.hideKeyboard();
		driver.findElementById("in.flatchat:id/ds_deposit_val").sendKeys(
				"15000");
		try {
			driver.hideKeyboard();
		} catch (Exception e) {
			System.out.println("Keyboard is already hidden..!");
		}
		driver.scrollToExact("FURNISHING");
		driver.findElementById("in.flatchat:id/radioSemi").click();
		DeleteTextFields("in.flatchat:id/description");
		driver.findElementById("in.flatchat:id/description").sendKeys(
				"Semi Furnished Rooms");
		try {
			driver.hideKeyboard();
		} catch (Exception e) {
			System.out.println("Keyboard is already hidden");
		}
		driver.scrollToExact("AMENITIES");
		driver.findElementById("in.flatchat:id/amenity_food").click();
		driver.findElementById("in.flatchat:id/amenity_internet").click();
		driver.findElementById("in.flatchat:id/amenity_power").click();
		driver.findElementById("in.flatchat:id/save_menu").click();
		Thread.sleep(5000);
		// wait.until(ExpectedConditions.visibilityOfElementLocated(By
		// .id("in.flatchat:id/title")));

	}

	public void EditProfile_Rooms() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageButton[@index='0']")));

		driver.findElementByAccessibilityId("Flatchat").click();
		try {
			driver.findElementByName("Edit Profile").click();
		} catch (Exception e) {
			driver.findElementByName("Create Profile").click();

		}
		driver.findElementById("in.flatchat:id/type_pg").click();
		driver.findElementById("in.flatchat:id/radioMaster").click();
		driver.findElementById("in.flatchat:id/radioBoth").click();
		driver.findElementById("in.flatchat:id/upload_image_btn").click();
		driver.findElementByName("Choose from Gallery").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("AllPhotos")));
		driver.findElementByName("AllPhotos").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageView")));

		List<AndroidElement> images = driver.findElements(By
				.xpath("//android.widget.ImageView"));
		((RemoteWebElement) images.get(2)).click();
		((RemoteWebElement) images.get(4)).click();
		((RemoteWebElement) images.get(5)).click();
		System.out.println(images.size());
		driver.findElementById("in.flatchat:id/btnGalleryOk").click();

		driver.findElementById("in.flatchat:id/loc_done").click();
		try {
			driver.scrollToExact("Monthly Rent");
		} catch (Exception e) {
			System.out.println("Rent alrady added..!");
		}
		driver.findElementById("in.flatchat:id/ss_rent_val").sendKeys("6000");
		try {
			driver.hideKeyboard();
		} catch (Exception e) {
			System.out.println("Keyboard is already hidden..!");
		}
		driver.findElementById("in.flatchat:id/ss_deposit_val").sendKeys(
				"18000");
		driver.hideKeyboard();
		driver.findElementById("in.flatchat:id/ds_rent_switch").click();
		driver.findElementById("in.flatchat:id/ds_rent_val").sendKeys("5000");
		driver.hideKeyboard();
		driver.findElementById("in.flatchat:id/ds_deposit_val").sendKeys(
				"15000");
		try {
			driver.hideKeyboard();
		} catch (Exception e) {
			System.out.println("Keyboard is already hidden..!");
		}
		driver.scrollToExact("FURNISHING");
		driver.findElementById("in.flatchat:id/radioSemi").click();
		DeleteTextFields("in.flatchat:id/description");
		driver.findElementById("in.flatchat:id/description").sendKeys(
				"Semi Furnished Rooms");
		try {
			driver.hideKeyboard();
		} catch (Exception e) {
			System.out.println("Keyboard is already hidden");
		}
		driver.scrollToExact("AMENITIES");
		driver.findElementById("in.flatchat:id/amenity_food").click();
		driver.findElementById("in.flatchat:id/amenity_internet").click();
		driver.findElementById("in.flatchat:id/amenity_power").click();
		driver.findElementById("in.flatchat:id/save_menu").click();
		Thread.sleep(5000);
		// wait.until(ExpectedConditions.visibilityOfElementLocated(By
		// .id("in.flatchat:id/title")));

	}

}

package ownerProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class I_CheckUserDetails extends BaseTestCase {

	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Owner\\I_CheckUserDetails";
	public static String CHAT = "Hello there, can we chat?";

	public I_CheckUserDetails() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Test
	public void checkUserDetails() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/title")));
		driver.findElementById("in.flatchat:id/title").click();
		Thread.sleep(2000);
		driver.sendKeyEvent(4);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/title")));
		driver.findElementById("in.flatchat:id/title").click();
		File image = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image, new File(folderPath + "\\User_details1.png"));

		try {
			driver.scrollToExact("DESCRIPTION");
		} catch (Exception e) {
			e.printStackTrace();
			driver.scrollToExact("PREFERRED LOCALITIES");
		}
		Thread.sleep(2000);
		File image1 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils
				.copyFile(image1, new File(folderPath + "\\User_details2.png"));

	}
}
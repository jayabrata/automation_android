package ownerProfile;

import io.appium.java_client.android.AndroidElement;

import java.io.File;
import java.net.MalformedURLException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class C_CreateOwnerProfile_Room extends BaseTestCase {

	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Owner\\C_CreateProfile";

	public C_CreateOwnerProfile_Room() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	public int tapOnResult(String className) throws InterruptedException {
		return super.tapOnResult(className);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();

	}

	@Parameters({ "Country" })
	@Test
	public void runCreateProifle(String c) throws Exception {

		if (c.equals("India")) {
			createProfile_IN();
		} else {
			createProfile_room_SG();
		}
	}

	public void createProfile_IN() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("in.flatchat:id/country_code")));
			String cc = driver.findElementById("in.flatchat:id/country_code")
					.getText();
			System.out.println(cc);
			driver.findElementById("in.flatchat:id/phone").sendKeys(
					"7483121154");
			driver.findElementByName("Submit");
		} catch (Exception e) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.xpath("//android.widget.ImageButton[@index='0']")));
		}
		driver.findElementByAccessibilityId("Flatchat").click();
		try {
			driver.findElementByName("Edit Profile").click();
		} catch (Exception e) {
			driver.findElementByName("Create Profile").click();

		}
		driver.findElementById("in.flatchat:id/type_flat").click();
		driver.findElementById("in.flatchat:id/btn_2").click();
		driver.findElementById("in.flatchat:id/upload_image_btn").click();
		driver.findElementByName("Choose from Gallery").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("AllPhotos")));
		driver.findElementByName("AllPhotos").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageView")));

		List<AndroidElement> images = driver.findElements(By
				.xpath("//android.widget.ImageView"));
		((RemoteWebElement) images.get(0)).click();
		((RemoteWebElement) images.get(1)).click();
		((RemoteWebElement) images.get(3)).click();
		driver.findElementById("in.flatchat:id/btnGalleryOk").click();

		driver.findElementById("in.flatchat:id/loc_done").click();
		driver.findElementById("in.flatchat:id/location_box2").click();
		Thread.sleep(3000);
		driver.findElementById("in.flatchat:id/right_cross").click();
		driver.findElementById("in.flatchat:id/autocomplete_location")
				.sendKeys("Kanyakumari");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.className("android.view.View")));
		Thread.sleep(3000);
		tapOnResult("android.view.View");
		Thread.sleep(3000);
		try {
			driver.findElementById("in.flatchat:id/loc_done").click();
		} catch (Exception e) {
			tapOnResult("android.view.View");
			Thread.sleep(3000);
			driver.findElementById("in.flatchat:id/loc_done").click();
		}
		driver.scrollToExact("DETAILS");
		DeleteTextFields("in.flatchat:id/rent_val");
		driver.findElementById("in.flatchat:id/rent_val").sendKeys("10000");
		DeleteTextFields("in.flatchat:id/deposit_val");
		driver.findElementById("in.flatchat:id/deposit_val").sendKeys("70000");
		try {
			driver.hideKeyboard();
		} catch (Exception e) {
			System.out.println("Keyboard is already hidden..!");
		}
		driver.scrollToExact("DESCRIPTION");
		driver.findElementById("in.flatchat:id/radioSemi").click();
		driver.findElementById("in.flatchat:id/description").sendKeys(
				"Semi furnished House");
		driver.findElementById("in.flatchat:id/save_menu").click();
		File image = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image, new File(folderPath
				+ "\\Owner_AfterCreateProfile.png"));

	}

	public void createProfile_room_SG() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("in.flatchat:id/country_code")));
			String cc = driver.findElementById("in.flatchat:id/country_code")
					.getText();
			System.out.println(cc);
			driver.findElementById("in.flatchat:id/phone").sendKeys("81234567");
			driver.findElementByName("Submit");
		} catch (Exception e) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.xpath("//android.widget.ImageButton[@index='0']")));
		}
		driver.findElementByAccessibilityId("Flatchat").click();
		try {
			driver.findElementByName("Edit Profile").click();
		} catch (Exception e) {
			driver.findElementByName("Create Profile").click();

		}
		driver.findElementById("in.flatchat:id/type_flat").click();
		driver.findElementById("in.flatchat:id/btn_2").click();
		driver.findElementById("in.flatchat:id/radioLanded").click();
		driver.findElementById("in.flatchat:id/upload_image_btn").click();
		driver.findElementByName("Choose from Gallery").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("AllPhotos")));
		driver.findElementByName("AllPhotos").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageView")));

		List<AndroidElement> images = driver.findElements(By
				.xpath("//android.widget.ImageView"));
		((RemoteWebElement) images.get(0)).click();
		((RemoteWebElement) images.get(1)).click();
		((RemoteWebElement) images.get(3)).click();
		System.out.println(images.size());
		driver.findElementById("in.flatchat:id/btnGalleryOk").click();

		driver.findElementById("in.flatchat:id/loc_done").click();
		driver.findElementById("in.flatchat:id/location_box2").click();
		Thread.sleep(3000);
		driver.findElementById("in.flatchat:id/right_cross").click();
		driver.findElementById("in.flatchat:id/autocomplete_location")
				.sendKeys("Victoria");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.className("android.view.View")));
		Thread.sleep(3000);
		tapOnResult("android.view.View");
		Thread.sleep(3000);
		try {
			driver.findElementById("in.flatchat:id/loc_done").click();
		} catch (Exception e) {
			tapOnResult("android.view.View");
			Thread.sleep(3000);
			driver.findElementById("in.flatchat:id/loc_done").click();
		}
		driver.scrollToExact("PLACE DETAILS");
		DeleteTextFields("in.flatchat:id/rent_val");
		driver.findElementById("in.flatchat:id/rent_val").sendKeys("5000");
		DeleteTextFields("in.flatchat:id/deposit_val");
		driver.findElementById("in.flatchat:id/deposit_val").sendKeys("20000");
		DeleteTextFields("in.flatchat:id/sqft_area");
		driver.findElementById("in.flatchat:id/sqft_area").sendKeys("1000");
		try {
			driver.hideKeyboard();
		} catch (Exception e) {
			System.out.println("Keyboard is already hidden..!");
		}
		driver.scrollToExact("DESCRIPTION");
		driver.findElementById("in.flatchat:id/radioSemi").click();
		DeleteTextFields("in.flatchat:id/description");
		driver.findElementById("in.flatchat:id/description").sendKeys(
				"Semi furnished House");
		driver.findElementById("in.flatchat:id/save_menu").click();
		File image = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image, new File(folderPath
				+ "\\Owner_AfterCreateProfile.png"));

	}
}
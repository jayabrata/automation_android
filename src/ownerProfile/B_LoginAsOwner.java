package ownerProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import seleniumPart.VerificationCodeRetrieve;
import AppiumServer.BaseTestCase;

public class B_LoginAsOwner extends BaseTestCase {

	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Owner\\B_LoginAsOwner";
	String Gender = "Male";

	public B_LoginAsOwner() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Parameters({ "Country" })
	@Test
	public void runCreateProifle(String c) throws Exception {

		if (c.equals("India")) {
			loginAsOwner_IN();
		} else {
			loginAsOwner_SG();
		}
	}

	public void loginAsOwner_IN() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		if (wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/india_layout"))) != null) {
			driver.findElementById("in.flatchat:id/india_layout").click();
		}
		driver.findElementById("in.flatchat:id/owner_logo").click();
		driver.findElementById("in.flatchat:id/google_btn").click();
		Thread.sleep(3000);
		try {
			if (driver.findElement(By.id("android:id/title")) != null) {
				File image1 = ((TakesScreenshot) driver)
						.getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(image1, new File(folderPath
						+ "\\LoggingInWithGoogle.png"));
				driver.findElementById(
						"com.google.android.gms:id/account_display_name")
						.click();
			}

			Thread.sleep(2000);
			// driver.findElementByName("Select gender").click();
			// driver.findElementByName(Gender).click();
			// driver.findElementById("com.google.android.gms:id/next_button")
			// .click();
			// Thread.sleep(10000);
			// File image2 = ((TakesScreenshot) driver)
			// .getScreenshotAs(OutputType.FILE);
			// FileUtils.copyFile(image2, new File(folderPath
			// + "\\AccessPermissionFromGoogle.png"));
			// driver.findElementById("com.google.android.gms:id/accept_button")
			// .click();
			// Thread.sleep(10000);
			driver.findElementById("in.flatchat:id/skip_btn").click();
			Thread.sleep(5000);
			File image3 = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(image3, new File(folderPath
					+ "\\EditProfileLandingPage.png"));

		} catch (Exception e) {
			try {
				wait.until(ExpectedConditions.elementToBeClickable(By
						.name("Edit Profile")));
			} catch (Exception e1) {

			}

		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/country_code")));
		String cc = driver.findElementById("in.flatchat:id/country_code")
				.getText();
		if (cc.equals("+91")) {
			System.out.println("Located for India");
			File image4 = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(image4, new File(folderPath
					+ "\\AddPhoneNumber.png"));
		} else {

		}
	}

	public void loginAsOwner_SG() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 20);
		if (wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/singapore_layout"))) != null) {
			driver.findElementById("in.flatchat:id/singapore_layout").click();
		}
		driver.findElementById("in.flatchat:id/owner_logo").click();
		driver.findElementById("in.flatchat:id/google_btn").click();
		Thread.sleep(3000);
		try {
			if (driver.findElement(By.id("android:id/title")) != null) {
				File image1 = ((TakesScreenshot) driver)
						.getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(image1, new File(folderPath
						+ "\\LoggingInWithGoogle.png"));
				driver.findElementById(
						"com.google.android.gms:id/account_display_name")
						.click();
			}
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.name("8-digit phone number")));
			driver.findElementById("in.flatchat:id/phone_number_edit_text")
					.sendKeys("81234567");
			driver.findElementById("in.flatchat:id/send_button").click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("in.flatchat:id/save_menu")));
			File image3 = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(image3, new File(folderPath
					+ "\\EditProfileLandingPage.png"));
			driver.sendKeyEvent(4);

		} catch (Exception e) {
			try {
				wait.until(ExpectedConditions.elementToBeClickable(By
						.name("CHATS")));
			} catch (Exception e1) {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//android.widget.ImageButton[@index='0']")));
				driver.findElementByAccessibilityId("Flatchat").click();
				driver.findElementByName("Verify Phone Number").click();
				String code = VerificationCodeRetrieve.retrieveCode("81234567");
				driver.findElementByName("Enter Verification Code").sendKeys(
						code);
				driver.findElementByName("Verify").click();
				try {
					wait.until(ExpectedConditions.visibilityOfElementLocated(By
							.name("Please create your profile from the sidebar to see matches.")));
					System.out
							.println("Phone number verification is Successfull..!!");
				} catch (Exception e2) {
					System.out
							.println("Phone number verification is Unsuccessfull..!!");
				}

			}

		}

	}
}

package tenantProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class K_ShareOwnProifle extends BaseTestCase {

	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Tenant\\K_ShareOwnProifle";

	public K_ShareOwnProifle() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		// TODO Auto-generated method stub
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Test
	public void shareOwnProfile() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/title")));
		driver.findElementByAccessibilityId("Flatchat").click();
		driver.findElementByName("Share").click();
		driver.findElementByName("Gmail").click();
		Thread.sleep(3000);
		File image1 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image1,
				new File(folderPath + "\\SharedProfile2.png"));
		driver.closeApp();
		driver.launchApp();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/title")));
		driver.findElementByAccessibilityId("Flatchat").click();
		driver.findElementById("in.flatchat:id/ppic").click();
		driver.findElementById("in.flatchat:id/fab_bottom").click();
		driver.findElementByName("Gmail").click();
		Thread.sleep(3000);
		File image2 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image2,
				new File(folderPath + "\\SharedProfile2.png"));

	}
}

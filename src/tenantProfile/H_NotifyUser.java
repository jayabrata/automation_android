package tenantProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class H_NotifyUser extends BaseTestCase {
	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Tenant\\H_NotifyUser";
	public static String CHAT = "Hello there, can we chat?";

	public H_NotifyUser() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		// TODO Auto-generated method stub
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Test
	public void notifyUser() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/title")));
		driver.findElementById("in.flatchat:id/title").click();
		Thread.sleep(2000);
		driver.sendKeyEvent(4);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/title")));
		String nameInMatches = driver.findElementById("in.flatchat:id/title")
				.getText();
		System.out.println(nameInMatches);
		driver.findElementById("in.flatchat:id/title").click();
		driver.findElementById("in.flatchat:id/fab_top").click();
		driver.findElementById("in.flatchat:id/msg").sendKeys(CHAT);
		driver.findElementById("in.flatchat:id/send_btn").click();
		Thread.sleep(5000);
		driver.sendKeyEvent(4);
		driver.findElementByName("CHATS").click();
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("in.flatchat:id/introduce_text")));
			driver.findElementById("in.flatchat:id/btn_got_it").click();
		} catch (Exception e) {
		}
		try {
			driver.scrollToExact(nameInMatches);
			driver.findElementByName(nameInMatches).click();
			if ((driver.findElementByName("Awaiting user response") != null)) {
				File image = ((TakesScreenshot) driver)
						.getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(image, new File(folderPath
						+ "\\User_notifed_inChat.png"));

			}
		} catch (Exception e) {
			System.out.println("User is not properly notified..!");

		}
		File image = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image, new File(folderPath
				+ "\\User_notifed_inChat.png"));
	}
}

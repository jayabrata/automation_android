package tenantProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class B_LoginAsTenant extends BaseTestCase {
	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Tenant\\B_LoginAsTenant";
	String Gender = "Male";

	public B_LoginAsTenant() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		// TODO Auto-generated method stub
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Parameters({ "Country" })
	@Test
	public void runCreateProifle(String c) throws Exception {

		if (c.equals("India")) {
			loginAsTenant_IN();
		} else {
			loginAsTenant_SG();
		}
	}

	public void loginAsTenant_IN() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		if (wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/india_layout"))) != null) {
			driver.findElementById("in.flatchat:id/india_layout").click();
		}
		driver.findElementById("in.flatchat:id/tenant_logo").click();
		driver.findElementById("in.flatchat:id/google_btn").click();
		Thread.sleep(3000);
		try {
			if (driver.findElement(By.id("android:id/title")) != null) {
				File image1 = ((TakesScreenshot) driver)
						.getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(image1, new File(folderPath
						+ "\\LoggingInWithGoogle.png"));
				driver.findElementById(
						"com.google.android.gms:id/account_display_name")
						.click();
			}

			Thread.sleep(2000);
			// driver.findElementByName("Select gender").click();
			// driver.findElementByName(Gender).click();
			// driver.findElementById("com.google.android.gms:id/next_button")
			// .click();
			// SThread.sleep(10000);
			// File image2 = ((TakesScreenshot) driver)
			// .getScreenshotAs(OutputType.FILE);
			// FileUtils.copyFile(image2, new File(folderPath
			// + "\\AccessPermissionFromGoogle.png"));
			// driver.findElementById("com.google.android.gms:id/accept_button")
			// .click();
			// Thread.sleep(10000);
			driver.findElementById("in.flatchat:id/skip_btn").click();
			Thread.sleep(5000);
			File image3 = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(image3, new File(folderPath
					+ "\\EditProfileLandingPage.png"));

		} catch (Exception e) {
			try {
				wait.until(ExpectedConditions.elementToBeClickable(By
						.name("CHATS")));
			} catch (Exception e1) {

			}

		}
		driver.closeApp();

	}

	public void loginAsTenant_SG() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		if (wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/india_layout"))) != null) {
			driver.findElementById("in.flatchat:id/singapore_layout").click();
		}
		driver.findElementById("in.flatchat:id/tenant_logo").click();
		driver.findElementById("in.flatchat:id/google_btn").click();
		Thread.sleep(3000);
		try {
			if (driver.findElement(By.id("android:id/title")) != null) {
				File image1 = ((TakesScreenshot) driver)
						.getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(image1, new File(folderPath
						+ "\\LoggingInWithGoogle.png"));
				driver.findElementById(
						"com.google.android.gms:id/account_display_name")
						.click();
			}

			Thread.sleep(2000);
			// driver.findElementByName("Select gender").click();
			// driver.findElementByName(Gender).click();
			// driver.findElementById("com.google.android.gms:id/next_button")
			// .click();
			// SThread.sleep(10000);
			// File image2 = ((TakesScreenshot) driver)
			// .getScreenshotAs(OutputType.FILE);
			// FileUtils.copyFile(image2, new File(folderPath
			// + "\\AccessPermissionFromGoogle.png"));
			// driver.findElementById("com.google.android.gms:id/accept_button")
			// .click();
			// Thread.sleep(10000);
			driver.findElementById("in.flatchat:id/skip_btn").click();
			Thread.sleep(5000);
			File image3 = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(image3, new File(folderPath
					+ "\\EditProfileLandingPage.png"));

		} catch (Exception e) {
			try {
				wait.until(ExpectedConditions.elementToBeClickable(By
						.name("CHATS")));
			} catch (Exception e1) {

			}

		}
		driver.closeApp();

	}

}

package tenantProfile;

import io.appium.java_client.TouchAction;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class M_deleteGroupMember extends BaseTestCase {
	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Tenant\\M_deleteGroupMember";
	public static String groupName = "New Group Automation";
	TouchAction ta;

	public M_deleteGroupMember() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		// TODO Auto-generated method stub
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Test
	public void deleteGroupMember() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("CHATS")));
		driver.findElementByName("CHATS").click();
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("in.flatchat:id/introduce_text")));
			driver.findElementById("in.flatchat:id/btn_got_it").click();
		} catch (Exception e) {
		}
		driver.findElementByName(groupName).click();
		driver.findElementByAccessibilityId("More options").click();
		driver.findElementByName("Group details").click();
		WebElement element = driver
				.findElementByXPath("//android.widget.LinearLayout[@index='1']");
		TouchAction ta = new TouchAction(driver);
		ta.longPress(element).perform();
		driver.findElementByName("Remove User").click();

		Thread.sleep(3000);
		File image = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image, new File(folderPath + "\\User_deleted.png"));
	}
}
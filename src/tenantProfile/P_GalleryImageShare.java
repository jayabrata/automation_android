package tenantProfile;

import io.appium.java_client.android.AndroidElement;

import java.io.File;
import java.net.MalformedURLException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class P_GalleryImageShare extends BaseTestCase {
	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Tenant\\P_GalleryImageShare";

	public P_GalleryImageShare() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		// TODO Auto-generated method stub
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Test
	public void contactShare() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("CHATS")));
		driver.findElementByName("CHATS").click();
		// driver.scrollToExact("Jaybrata Chakraborty");
		driver.findElementByName("Jaybrata Chakraborty").click();
		driver.findElementById("in.flatchat:id/ready_msg").click();
		driver.findElementByName("Gallery").click();
		driver.findElementByName("AllPhotos").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageView")));

		List<AndroidElement> images = driver.findElements(By
				.xpath("//android.widget.ImageView"));
		((RemoteWebElement) images.get(0)).click();
		((RemoteWebElement) images.get(1)).click();
		((RemoteWebElement) images.get(3)).click();
		System.out.println(images.size());
		driver.findElementById("in.flatchat:id/btnGalleryOk").click();
		Thread.sleep(5000);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("in.flatchat:id/delivery_status")));
			System.out.println("Multiple shared successfully..!");
		} catch (Exception e) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("in.flatchat:id/chat_resend_btn")));
			System.out.println("Multiple Share failed.!!");
		}
		File image = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image, new File(folderPath
				+ "\\MultipleImagesShared.png"));

	}
}

package tenantProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class E_CheckingMatches extends BaseTestCase {
	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Tenant\\E_CheckingMatches";

	public E_CheckingMatches() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		// TODO Auto-generated method stub
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Test
	public void matchesView() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);

		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("in.flatchat:id/btn_got_it")));
			driver.findElementById("in.flatchat:id/btn_got_it").click();
		} catch (Exception e) {
			if (driver.findElementById("in.flatchat:id/pic") != null
					|| driver
							.findElementById("in.flatchat:id/rent_or_budget_val") != null) {
				File image = ((TakesScreenshot) driver)
						.getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(image, new File(folderPath
						+ "\\Tenant_Matches.png"));
			}
		}
		File image = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils
				.copyFile(image, new File(folderPath + "\\Tenant_Matches.png"));
	}

}

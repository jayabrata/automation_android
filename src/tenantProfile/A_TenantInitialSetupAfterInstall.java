package tenantProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class A_TenantInitialSetupAfterInstall extends BaseTestCase {

	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Tenant\\A_InitialSetupAfterInstall";

	public A_TenantInitialSetupAfterInstall() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		// TODO Auto-generated method stub
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Test
	public void initialSetup() throws Exception {
		seleniumPart.PatronDelete.deleteOfPatron();
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/arrow")));
		File image1 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image1, new File(folderPath
				+ "\\FirstPageAnimation.png"));
		driver.findElementById("in.flatchat:id/arrow").click();
		driver.findElementById("in.flatchat:id/arrow").click();
		Thread.sleep(3000);
		driver.findElementById("in.flatchat:id/arrow").click();
		driver.findElementById("in.flatchat:id/loginactivitybtn").click();
		Thread.sleep(3000);
		File image2 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image2, new File(folderPath
				+ "\\InitialSetupComplete.png"));

	}

}

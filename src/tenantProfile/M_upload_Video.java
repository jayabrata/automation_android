package tenantProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class M_upload_Video extends BaseTestCase {

	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Tenant\\J_upload_Video";
	public static String CHAT = "Hello there, can we chat?";

	public M_upload_Video() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Test
	public void uploadVideo() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("CHATS")));
		driver.findElementByAccessibilityId("Flatchat").click();
		driver.findElementByName("Edit Profile").click();
		driver.scrollToExact("VIDEO (OPTIONAL)");

		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.name("Introduce yourself, add a video")));
			driver.findElementByName("Introduce yourself, add a video").click();
			driver.findElementById(
					"com.android.gallery3d:id/shutter_button_video").click();
			Thread.sleep(15000);
			driver.findElementById("com.android.gallery3d:id/camera_shutter")
					.click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("com.android.gallery3d:id/btn_play")));
			driver.findElementById("com.android.gallery3d:id/btn_done").click();
			Thread.sleep(20000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.name("Play your video")));
			driver.findElementById("in.flatchat:id/save_menu").click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("in.flatchat:id/action_close")));
			driver.findElementById("in.flatchat:id/action_close").click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.name("CHATS")));
			driver.findElementByAccessibilityId("Flatchat").click();
			driver.findElementById("in.flatchat:id/ppic").click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("in.flatchat:id/cross_reload")));
			System.out.println("Video Uploaded successflly..!");

		} catch (Exception e) {
			System.out.println("Video is uploaded already..!");
		}
		Thread.sleep(2000);
		File image1 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image1,
				new File(folderPath + "\\Video_uploaded.png"));

	}
}
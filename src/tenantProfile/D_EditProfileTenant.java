package tenantProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class D_EditProfileTenant extends BaseTestCase {
	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Tenant\\D_EditProfileTenant";

	public D_EditProfileTenant() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	public void DeleteTextFields(String Id) {
		// TODO Auto-generated method stub
		super.DeleteTextFields(Id);
	}

	@Override
	public int tapOnResult(String className) throws InterruptedException {
		// TODO Auto-generated method stub
		return super.tapOnResult(className);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		// TODO Auto-generated method stub
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Parameters({ "Country" })
	@Test
	public void runCreateProifle(String c) throws Exception {

		if (c.equals("India")) {
			TenantEditProfile_IN();
		} else {
			TenantEditProfile_SG();
		}
	}

	public void TenantEditProfile_IN() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageButton[@index='0']")));
		driver.findElementByXPath("//android.widget.ImageButton[@index='0']")
				.click();
		driver.findElementByName("Flyer").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/share_flyer")));
		File image1 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image1, new File(folderPath
				+ "\\Tenant_FlyerBeforeEditProfile.png"));

		driver.findElementById("in.flatchat:id/cancel").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageButton[@index='0']")));
		driver.findElementByXPath("//android.widget.ImageButton[@index='0']")
				.click();
		driver.findElementByName("Edit Profile").click();
		DeleteTextFields("in.flatchat:id/rent_val");
		driver.findElementById("in.flatchat:id/rent_val").sendKeys("7000");
		driver.hideKeyboard();
		DeleteTextFields("in.flatchat:id/deposit_val");
		driver.findElementById("in.flatchat:id/deposit_val").sendKeys("70000");
		driver.hideKeyboard();
		Thread.sleep(1000);
		driver.findElement(By.id("in.flatchat:id/location_box2")).click();
		Thread.sleep(3000);
		driver.findElementById("in.flatchat:id/right_cross").click();
		driver.findElementById("in.flatchat:id/autocomplete_location")
				.sendKeys("Kanyakumari");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.className("android.view.View")));
		tapOnResult("android.view.View");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/loc_done")));
		driver.findElementById("in.flatchat:id/loc_done").click();
		driver.scrollToExact("DESCRIPTION");
		driver.findElementByName("Family-Person").click();
		driver.findElementByName("Book-Lover").click();
		driver.findElementByName("Love Cooking").click();
		driver.findElementByName("Hygiene-Freak").click();
		driver.findElementByName("Foodie").click();
		driver.findElementByName("Bachelor").click();
		DeleteTextFields("in.flatchat:id/description");
		driver.findElementById("in.flatchat:id/description").sendKeys(
				"Test Profile Tenant");
		driver.findElementById("in.flatchat:id/save_menu").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/share_flyer")));
		File image2 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image2, new File(folderPath
				+ "\\Tenant_FlyerAfterEditProfile.png"));

		driver.findElementById("in.flatchat:id/cancel").click();

	}

	public void TenantEditProfile_SG() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageButton[@index='0']")));
		driver.findElementByXPath("//android.widget.ImageButton[@index='0']")
				.click();
		driver.findElementByName("Flyer").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/share_flyer")));
		File image1 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image1, new File(folderPath
				+ "\\Tenant_FlyerBeforeEditProfile.png"));

		driver.findElementById("in.flatchat:id/cancel").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageButton[@index='0']")));
		driver.findElementByXPath("//android.widget.ImageButton[@index='0']")
				.click();
		driver.findElementByName("Edit Profile").click();
		driver.findElementById("in.flatchat:id/radioBoth").click();
		DeleteTextFields("in.flatchat:id/rent_val");
		driver.findElementById("in.flatchat:id/rent_val").sendKeys("7000");
		driver.hideKeyboard();
		DeleteTextFields("in.flatchat:id/deposit_val");
		driver.findElementById("in.flatchat:id/deposit_val").sendKeys("70000");
		Thread.sleep(1000);
		driver.hideKeyboard();
		driver.findElement(By.id("in.flatchat:id/location_box2")).click();
		Thread.sleep(3000);
		driver.findElementById("in.flatchat:id/right_cross").click();
		driver.findElementById("in.flatchat:id/autocomplete_location")
				.sendKeys("Victoria");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.className("android.view.View")));
		tapOnResult("android.view.View");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/loc_done")));
		driver.findElementById("in.flatchat:id/loc_done").click();
		driver.scrollToExact("DESCRIPTION");
		driver.findElementByName("Family-Person").click();
		driver.findElementByName("Book-Lover").click();
		driver.findElementByName("Love Cooking").click();
		driver.findElementByName("Hygiene-Freak").click();
		driver.findElementByName("Foodie").click();
		driver.findElementByName("Bachelor").click();
		DeleteTextFields("in.flatchat:id/description");
		driver.findElementById("in.flatchat:id/description").sendKeys(
				"Test Profile Tenant");
		driver.findElementById("in.flatchat:id/save_menu").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/share_flyer")));
		File image2 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image2, new File(folderPath
				+ "\\Tenant_FlyerAfterEditProfile.png"));

		driver.findElementById("in.flatchat:id/cancel").click();

	}
}
package tenantProfile;

import io.appium.java_client.android.AndroidElement;

import java.io.File;
import java.net.MalformedURLException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class L_EditGroup extends BaseTestCase {
	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Tenant\\L_EditGroup";
	public static String groupName = "New Group Automation";

	public L_EditGroup() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		// TODO Auto-generated method stub
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Test
	public void editGroup() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("CHATS")));
		driver.findElementByName("CHATS").click();
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("in.flatchat:id/introduce_text")));
			driver.findElementById("in.flatchat:id/btn_got_it").click();
		} catch (Exception e) {
		}
		driver.findElementByName("Automation Group").click();
		driver.findElementByAccessibilityId("More options").click();
		driver.findElementByName("Group details").click();
		Thread.sleep(2000);
		File image1 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image1, new File(folderPath
				+ "\\groupBeforeEdit.png"));
		driver.findElementByAccessibilityId("More options").click();
		driver.findElementByName("Edit Info").click();
		driver.findElementById("in.flatchat:id/group_image").click();
		driver.findElementByName("AllPhotos").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageView")));
		List<AndroidElement> images = driver.findElements(By
				.xpath("//android.widget.ImageView"));
		((RemoteWebElement) images.get(3)).click();

		driver.findElementById("in.flatchat:id/crp_done").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/group_name")));
		DeleteTextFields("in.flatchat:id/group_name");
		driver.findElementById("in.flatchat:id/group_name").sendKeys(
				"New Group Automation");
		driver.findElementById("in.flatchat:id/next_menu").click();

		if (groupName.equals(driver.findElementById("in.flatchat:id/user_name")
				.getText())) {
			System.out.println("Group Edited Successfully..!");
		}
		Thread.sleep(5000);
		File image = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image, new File(folderPath
				+ "\\groupAfterEdited.png"));

	}
}

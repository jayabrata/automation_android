package tenantProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class P_LoactionShare extends BaseTestCase {
	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Tenant\\P_LoactionShare";

	public P_LoactionShare() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		// TODO Auto-generated method stub
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Test
	public void contactShare() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("CHATS")));
		driver.findElementByName("CHATS").click();
		// driver.scrollToExact("Jaybrata Chakraborty");
		driver.findElementByName("Jaybrata Chakraborty").click();
		driver.findElementById("in.flatchat:id/ready_msg").click();
		driver.findElementByName("Location").click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/right_cross")));
		driver.findElementById("in.flatchat:id/right_cross").click();
		driver.findElementById("in.flatchat:id/autocomplete_location")
				.sendKeys("Kanyakumari");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.className("android.view.View")));
		tapOnResult("android.view.View");
		Thread.sleep(2000);
		driver.findElementById("in.flatchat:id/loc_done").click();
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("in.flatchat:id/delivery_status")));
			System.out.println("Loaction shared successfully..!");
		} catch (Exception e) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("in.flatchat:id/chat_resend_btn")));
			System.out.println("Loaction Share failed.!!");
		}
		File image = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils
				.copyFile(image, new File(folderPath + "\\LocationShared.png"));

	}
}
package tenantProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class N_DeleteVideo extends BaseTestCase {

	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Tenant\\N_DeleteVideo";
	public static String CHAT = "Hello there, can we chat?";

	public N_DeleteVideo() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Test
	public void deleteVideo() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("CHATS")));
		driver.findElementByAccessibilityId("Flatchat").click();
		driver.findElementByName("Edit Profile").click();
		driver.scrollToExact("VIDEO (OPTIONAL)");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("Play your video")));
		File image1 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils
				.copyFile(image1, new File(folderPath + "\\Video_presnet.png"));
		deleteProfileVideo("in.flatchat:id/video_upload_text");
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.name("Remove video")));
			driver.findElementById("android:id/button2").click();
		} catch (Exception e) {
			System.out.println("Cross button not tapped properly..!");
		}

		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("Add a video message.")));
		System.out.println("Video deleted successfully..!");
		Thread.sleep(2000);
		File image2 = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils
				.copyFile(image2, new File(folderPath + "\\Video_deleted.png"));
		driver.findElementById("in.flatchat:id/save_menu").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/action_close")));
		driver.findElementById("in.flatchat:id/action_close").click();

	}
}
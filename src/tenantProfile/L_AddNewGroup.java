package tenantProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class L_AddNewGroup extends BaseTestCase {
	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Tenant\\L_AddNewGroup";
	public static String CHAT = "Hello there, can we chat?";

	public L_AddNewGroup() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		// TODO Auto-generated method stub
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Test
	public void addNewGroup() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("CHATS")));
		driver.findElementByName("CHATS").click();
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("in.flatchat:id/introduce_text")));
			driver.findElementById("in.flatchat:id/btn_got_it").click();
			driver.findElementById("in.flatchat:id/fab_add_group").click();
		} catch (Exception e) {
			driver.findElementById("in.flatchat:id/fab_add_group").click();
		}
		driver.findElementById("in.flatchat:id/group_name").sendKeys(
				"Automation Group");
		driver.findElementById("in.flatchat:id/next_menu").click();
		driver.findElementByXPath("//android.widget.FrameLayout[@index='0']")
				.click();
		driver.findElementByXPath("//android.widget.FrameLayout[@index='1']")
				.click();
		driver.findElementByXPath("//android.widget.FrameLayout[@index='2']")
				.click();
		driver.findElementById("in.flatchat:id/save_menu").click();
		File image = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image, new File(folderPath + "\\Group_Added.png"));
	}
}

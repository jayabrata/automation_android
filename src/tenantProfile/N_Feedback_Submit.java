package tenantProfile;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class N_Feedback_Submit extends BaseTestCase {

	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Owner\\N_Feedback_Submit";
	public static String CHAT = "Hello there, can we chat?";

	public N_Feedback_Submit() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.quit();
	}

	@Test
	public void checkUserDetails() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageButton[@index='0']")));
		driver.findElementByAccessibilityId("Flatchat").click();

		driver.findElementByName("Feedback").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/feed_done")));
		driver.findElementById("in.flatchat:id/comments").sendKeys(
				"Testing Comments");
		driver.hideKeyboard();
		Thread.sleep(2000);
		driver.findElementById("in.flatchat:id/number_matches").click();
		driver.findElementById("in.flatchat:id/design").click();
		driver.findElementById("in.flatchat:id/technical_support").click();
		driver.findElementById("in.flatchat:id/features").click();
		driver.findElementById("in.flatchat:id/chat_exp").click();
		driver.findElementById("in.flatchat:id/quality_matches").click();
		driver.scrollToExact("How would you rate your experience?");
		submitRating_feedback("android.widget.RatingBar");
		driver.findElementById("in.flatchat:id/feed_done").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("We have received your feedback")));
		File image = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image, new File(folderPath
				+ "\\Feedback_Submittted.png"));
	}
}
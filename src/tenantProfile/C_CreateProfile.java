package tenantProfile;

import io.appium.java_client.android.AndroidElement;

import java.io.File;
import java.net.MalformedURLException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import AppiumServer.BaseTestCase;

public class C_CreateProfile extends BaseTestCase {
	String folderPath = "E:\\Automation_Setup\\WorkSpace\\FlatChat_India\\Tenant\\C_CreateProfile";
	String Class = "android.view.View";

	public C_CreateProfile() {
		BaseTestCase.CreateFolder(folderPath);
	}

	@Override
	public int tapOnResult(String className) throws InterruptedException {
		// TODO Auto-generated method stub
		return super.tapOnResult(className);
	}

	@Override
	public void DeleteTextFields(String Id) {
		// TODO Auto-generated method stub
		super.DeleteTextFields(Id);
	}

	@Override
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		// TODO Auto-generated method stub
		super.setUp();
	}

	@AfterMethod
	public void teardown() {
		driver.closeApp();
		driver.quit();
	}

	@Parameters({ "Country" })
	@Test
	public void runCreateProifle(String c) throws Exception {

		if (c.equals("India")) {
			CreateProfile_IN();
		} else {
			CreateProfile_SG();
		}
	}

	public void CreateProfile_IN() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageButton[@index='0']")));
		driver.findElementByAccessibilityId("Flatchat").click();

		try {
			driver.findElementByName("Edit Profile").click();
		} catch (Exception e) {
			driver.findElementByName("Create Profile").click();
		}
		DeleteTextFields("in.flatchat:id/rent_val");
		driver.findElementById("in.flatchat:id/rent_val").sendKeys("5000");
		driver.hideKeyboard();
		DeleteTextFields("in.flatchat:id/deposit_val");
		driver.findElementById("in.flatchat:id/deposit_val").sendKeys("60000");
		driver.hideKeyboard();
		driver.findElementById("in.flatchat:id/upload_image_btn").click();
		driver.findElementByName("Choose from Gallery").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("AllPhotos")));
		driver.findElementByName("AllPhotos").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageView")));

		List<AndroidElement> images = driver.findElements(By
				.xpath("//android.widget.ImageView"));
		((RemoteWebElement) images.get(0)).click();
		((RemoteWebElement) images.get(1)).click();
		((RemoteWebElement) images.get(3)).click();
		System.out.println(images.size());
		driver.findElementById("in.flatchat:id/btnGalleryOk").click();

		driver.findElementById("in.flatchat:id/loc_done").click();
		driver.findElement(By.id("in.flatchat:id/location_box2")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("in.flatchat:id/right_cross")).click();
		driver.findElementById("in.flatchat:id/autocomplete_location")
				.sendKeys("Kanyakumari");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.className("android.view.View")));
		tapOnResult("android.view.View");
		Thread.sleep(3000);
		driver.findElementById("in.flatchat:id/loc_done").click();
		driver.scrollTo("DESCRIPTION");
		driver.findElementByName("Family-Person").click();
		driver.findElementByName("Book-Lover").click();
		driver.findElementByName("Love Cooking").click();
		driver.findElementById("in.flatchat:id/description").sendKeys(
				"This is a test profile for Tenant");
		driver.findElementById("in.flatchat:id/save_menu").click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/share_flyer")));

		File image = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image, new File(folderPath
				+ "\\Tenant_FlyerAfterCreateProfile.png"));
		driver.findElementById("in.flatchat:id/cancel").click();
	}

	public void CreateProfile_SG() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageButton[@index='0']")));
		driver.findElementByAccessibilityId("Flatchat").click();

		try {
			driver.findElementByName("Edit Profile").click();
		} catch (Exception e) {
			driver.findElementByName("Create Profile").click();
		}
		DeleteTextFields("in.flatchat:id/rent_val");
		driver.findElementById("in.flatchat:id/rent_val").sendKeys("5000");
		driver.hideKeyboard();
		DeleteTextFields("in.flatchat:id/deposit_val");
		driver.findElementById("in.flatchat:id/deposit_val").sendKeys("60000");
		driver.hideKeyboard();
		driver.findElementById("in.flatchat:id/upload_image_btn").click();
		driver.findElementByName("Choose from Gallery").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("AllPhotos")));
		driver.findElementByName("AllPhotos").click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//android.widget.ImageView")));

		List<AndroidElement> images = driver.findElements(By
				.xpath("//android.widget.ImageView"));
		((RemoteWebElement) images.get(0)).click();
		((RemoteWebElement) images.get(1)).click();
		((RemoteWebElement) images.get(3)).click();
		System.out.println(images.size());
		driver.findElementById("in.flatchat:id/btnGalleryOk").click();

		driver.findElementById("in.flatchat:id/loc_done").click();
		driver.findElement(By.id("in.flatchat:id/location_box2")).click();
		Thread.sleep(3000);
		driver.findElementById("in.flatchat:id/right_cross").click();
		driver.findElementById("in.flatchat:id/autocomplete_location")
				.sendKeys("Vivo");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.className("android.view.View")));
		tapOnResult("android.view.View");
		Thread.sleep(3000);
		driver.findElementById("in.flatchat:id/loc_done").click();
		driver.scrollTo("DESCRIPTION");
		driver.findElementByName("Family-Person").click();
		driver.findElementByName("Book-Lover").click();
		driver.findElementByName("Love Cooking").click();
		driver.findElementById("in.flatchat:id/description").sendKeys(
				"This is a test profile for Tenant");
		driver.findElementById("in.flatchat:id/save_menu").click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("in.flatchat:id/share_flyer")));

		File image = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image, new File(folderPath
				+ "\\Tenant_FlyerAfterCreateProfile.png"));
		driver.findElementById("in.flatchat:id/cancel").click();
	}
}
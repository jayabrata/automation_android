package seleniumPart;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SendingSevenDayNotofication {
	static WebDriver driver;

	public static void sedingNotificationToDevice(String patronName)
			throws InterruptedException {
		String userName = null;

		driver = new FirefoxDriver();
		driver.get("http://dev.flat.to/admin/");
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("id_username")));
		driver.manage().window().maximize();
		driver.findElement(By.id("id_username")).sendKeys("qa");
		driver.findElement(By.id("id_password")).sendKeys("qaqaqa");
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		driver.findElement(By.linkText("Patrons")).click();
		driver.findElement(By.linkText(patronName)).click();

		userName = driver.findElement(By.id("id_username")).getAttribute(
				"value");
		System.out.println("Username for " + patronName + " is: " + userName);
		Thread.sleep(3000);
		driver.get("http://dev.flat.to/v2/deactivate_notification/" + userName
				+ "/");
		try {
			Thread.sleep(5000);
			// driver.getPageSource().contains("Done!");
			driver.close();
		} catch (Exception e) {
			System.out.println("Notificaion not sent...!");
		}
		Thread.sleep(3000);
	}

	public static void main(String[] args) throws InterruptedException {
		sedingNotificationToDevice("Jayabrata");
	}
}

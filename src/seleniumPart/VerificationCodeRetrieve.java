package seleniumPart;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class VerificationCodeRetrieve {
	static WebDriver driver;

	public static String retrieveCode(String phoneNumber) {
		String verifyCode = null;

		driver = new FirefoxDriver();
		driver.get("http://dev.flat.to/admin/");
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("id_username")));
		driver.manage().window().maximize();
		driver.findElement(By.id("id_username")).sendKeys("qa");
		driver.findElement(By.id("id_password")).sendKeys("qaqaqa");
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		driver.findElement(By.linkText("Phones")).click();
		try {
			driver.findElement(By.linkText(phoneNumber)).click();
			verifyCode = driver.findElement(By.id("id_code")).getAttribute(
					"value");
			System.out.println(verifyCode);
			Thread.sleep(3000);
			driver.close();
		} catch (Exception e) {
			driver.close();
		}

		return verifyCode;
	}

}

package seleniumPart;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PatronDelete {
	static WebDriver driver;

	public static void deleteOfPatron() {
		// JavascriptExecutor js = ((JavascriptExecutor) driver);

		driver = new FirefoxDriver();
		driver.get("http://dev.flat.to/admin/");
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("id_username")));
		driver.manage().window().maximize();
		driver.findElement(By.id("id_username")).sendKeys("qa");
		driver.findElement(By.id("id_password")).sendKeys("qaqaqa");
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		driver.findElement(By.linkText("Patrons")).click();
		try {
			driver.findElement(By.linkText("Jayabrata")).click();
			// js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			driver.findElement(By.linkText("Delete")).click();
			driver.findElement(By.xpath("//input[@type='submit']")).click();
			Thread.sleep(3000);
			driver.close();
		} catch (Exception e) {
			driver.close();
		}
	}

	public static void main(String[] args) {
		deleteOfPatron();
	}
}
